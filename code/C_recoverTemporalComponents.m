% file = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1118/preproc2/run-1/FEAT.feat/filtered_func_data.ica/melodic_mix';
% melodic_mix = load(file);
% 
% figure; imagesc(melodic_mix')
% 
% 
% file = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1118/preproc2/run-1/FEAT.feat/filtered_func_data.ica/melodic_Tmodes';
% melodic_Tmodes = load(file);
% 
% figure; imagesc(melodic_Tmodes')


%% get time-courses (non-z) for all components

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

% get IC timecourses

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B_PLS/T_tools/'];  addpath(genpath(pn.tools));

for indID = 1:numel(IDs)
    disp([IDs{indID}]);
    for indRun = 1:4
    try
        fname = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/',IDs{indID},'/preproc2/run-',num2str(indRun),'/FEAT.feat/filtered_func_data.ica/melodic_IC.nii.gz'];
        [ICs] = load_untouch_nii(fname);

        fname = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/',IDs{indID},'/preproc2/run-',num2str(indRun),'/',IDs{indID},'_run-',num2str(indRun),'_feat_detrended_highpassed.nii.gz'];
        [OrigData] = load_untouch_nii(fname);

        for idxIC = 1:30
            ICTime(idxIC,:) = nanmean(nanmean(nanmean(repmat(ICs.img(:,:,:,idxIC),1,1,1,1054).*OrigData.img,1),2),3);
        end;
        save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/J_temporalClustering/B_data/C_tempComps_', IDs{indID},'_run-',num2str(indRun),'.mat'], 'ICTime')
        
    catch
        disp(['Error: ', IDs{indID}, ' Run ', num2str(indRun)]);
    end
    end
end

% figure; imagesc(zscore(ICTime,[],2))
% 
% figure; imagesc(ICTime)