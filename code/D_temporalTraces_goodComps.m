% try to cluster ICA components across subjects to extract task structure components

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';

%% load info on components

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/A_clusterData_goodComps.mat', 'Z2', 'ICcomps', 'IdxComponent')

%% load relevant time series of components

for indIC = 1:size(IdxComponent,1)
    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/J_temporalClustering/B_data/C_tempComps_', IDs{IdxComponent(indIC,1)},'_run-',num2str(IdxComponent(indIC,2)),'.mat'], 'ICTime')
    ICcomps_raw(indIC,:) = ICTime(IdxComponent(indIC,3),:);
end

corrMat = corrcoef(zscore(ICcomps,[],2)');

numComps = 50;
T = cluster(Z2,'maxclust',numComps);
%[~, sortVal] = sort(T, 'ascend');
%figure; imagesc(corrMat(sortVal,sortVal),[-.3 .3])
% figure; 
% for indComp = 1:numComps
% 	subplot(numComps,1,indComp); plot(nanmean(ICcomps(T==indComp,:)));
% end

% sort T by correlation magnitude within cluster
CorrVal = [];
for indCluster = 1:numComps
    CorrVal(indCluster) = nanmean(nanmean(corrMat(T == indCluster,T == indCluster)));
end
[sortV, sortI] = sort(CorrVal,'descend');
T2 = T;
for indCluster = 1:numComps
   T(T2==sortI(indCluster)) = indCluster;
end

[~, sortVal] = sort(T, 'ascend');
figure; imagesc(corrMat(sortVal,sortVal),[-.3 .3])

figure;
for indComp = 1:50
    imagesc(ICcomps(T==indComp,:),[-1 1])
    pause(1)
end
imagesc(ICcomps(T==8,:),[-1 1])
imagesc(ICcomps(T==24,:),[-1 1])
imagesc(ICcomps(T==10,:),[-1 1])

h = figure('units','normalized','position',[.1 .1 .7 .8]);
for indComp = 1:16
    subplot(4,4,indComp)
    imagesc(ICcomps(T==indComp,:),[-1 1])
    ylabel('IC count'); xlabel('Time (vol.)')
    title(['Cluster ', num2str(indComp)])
end
suptitle('Temporal structure in spatial ICs')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


h = figure('units','normalized','position',[.1 .1 .7 .8]);
for indComp = 1:16
    subplot(4,4,indComp)
    imagesc(ICcomps_demeaned(T==indComp,:))
    ylabel('IC count'); xlabel('Time (vol.)')
    title(['Cluster ', num2str(indComp)])
end
suptitle('Temporal structure in spatial ICs')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% concatenate thalamic STIM components

h = figure('units','normalized','position',[.1 .1 .7 .8]);

    imagesc(zscore(cat(1,ICcomps(T==8,:),...
        ICcomps_demeaned(T==12,:),...
        ICcomps_demeaned(T==14,:),...
        ICcomps_demeaned(T==34,:)),[],2), [-1 1])
    ylabel('IC count'); xlabel('Time (vol.)')
suptitle('Temporal structure in spatial ICs')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% check the overlap between particular runs

IdxComponent(T==8,1:2)
IdxComponent(T==12,1:2)
IdxComponent(T==14,1:2)
IdxComponent(T==34,1:2)

N_8 = IdxComponent(T==8,1:2);
N_12 = IdxComponent(T==12,1:2);
N_14 = IdxComponent(T==14,1:2);
N_34 = IdxComponent(T==34,1:2);

N_2_unique = unique(IdxComponent(T==2,1:2), 'rows');

N_8_unique = unique(IdxComponent(T==8,1:2), 'rows');
N_12_unique = unique(IdxComponent(T==12,1:2), 'rows');
N_14_unique = unique(IdxComponent(T==14,1:2), 'rows');
N_34_unique = unique(IdxComponent(T==34,1:2), 'rows');

N_all_unique = unique(IdxComponent(ismember(T, [8,12,14,34]),1:2), 'rows');

N_2_8_unique = unique([N_2_unique; N_8_unique], 'rows');
N_2_all_unique = unique([N_2_unique; N_all_unique], 'rows');


add8Perc = (size(N_2_8_unique)-size(N_2_unique))./size(N_2_8_unique);
add2Perc = (size(N_2_8_unique)-size(N_8_unique))./size(N_2_8_unique);

size(N_2_all_unique)

size(N_8_unique)
size(N_12_unique)
size(N_14_unique)
size(N_34_unique)
size(N_all_unique)

size(N_8)
size(N_12)
size(N_14)

%% indicate task onsets, cut into trials

%% zscore within run (& subject), cut into different load conditions

taskStructure = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/1117_Run1_regressors.mat');
taskStructure.Regressors
taskStructure.RegressorInfo

% h = figure; hold on;
% plot(nanmean(ICcomps_raw(T==1,:)), 'LineWidth', 2, 'Color', [.8 .8 .8])
% plot(nanmean(ICcomps_raw(T==2,:)), 'LineWidth', 2, 'Color', [.7 .7 .7])
% plot(nanmean(ICcomps_raw(T==4,:)), 'LineWidth', 2, 'Color', [.6 .6 .6])
% plot(nanmean(ICcomps_raw(T==5,:)), 'LineWidth', 2, 'Color', [.5 .5 .5])
% plot(nanmean(ICcomps_raw(T==12,:)), 'LineWidth', 2, 'Color', [1 .3 .3])
% plot(taskStructure.Regressors(:,1))
% plot(taskStructure.Regressors(:,2))
% legend({'1'; '5'; '4'; '3'; 'DMN'})
% title('Temporal sequence of spatial networks')

CompSignal = [];
tmpTime = find(taskStructure.Regressors(:,2));
for indStim = 1:numel(tmpTime)
    CompSignal(1,indStim,:) = nanmean(ICcomps(T==1,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(2,indStim,:) = nanmean(ICcomps(T==2,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(3,indStim,:) = nanmean(ICcomps(T==5,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(4,indStim,:) = nanmean(ICcomps(T==4,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(5,indStim,:) = nanmean(ICcomps(T==12,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(6,indStim,:) = nanmean(ICcomps(T==8,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(7,indStim,:) = nanmean(ICcomps(T==9,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(8,indStim,:) = nanmean(ICcomps(T==13,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(9,indStim,:) = nanmean(ICcomps(T==15,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
end

time = [-3:25]*.645;
h = figure; hold on;
    thresh = 2; xl = [0+6 1+6];
    p1 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.8 .8 .8]); p1.EdgeColor = 'none';p1.FaceAlpha = .3;
    thresh = 2; xl = [1+6 3+6];
    p2 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.7 .7 .7]); p2.EdgeColor = 'none';p2.FaceAlpha = .3;
    thresh = 2; xl = [3+6 6+6];
    p3 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.6 .6 .6]); p3.EdgeColor = 'none';p3.FaceAlpha = .3;
    thresh = 2; xl = [6+6 8+6];
    p4 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.5 .5 .5]); p4.EdgeColor = 'none';p4.FaceAlpha = .3;
    thresh = 2; xl = [8+6 10+6];
    p5 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[1 .3 .3]); p5.EdgeColor = 'none';p5.FaceAlpha = .3;
%     plot(time,zscore(squeeze(nanmean(CompSignal(1,:,:),2))), 'LineWidth', 2, 'Color', [.8 .8 .8])
%     plot(time,zscore(squeeze(nanmean(CompSignal(2,:,:),2))), 'LineWidth', 2, 'Color', [.7 .7 .7])
%     plot(time,zscore(squeeze(nanmean(CompSignal(3,:,:),2))), 'LineWidth', 2, 'Color', [.6 .6 .6])
%     plot(time,zscore(squeeze(nanmean(CompSignal(4,:,:),2))), 'LineWidth', 2, 'Color', [.5 .5 .5])
    plot(time,zscore(squeeze(nanmean(CompSignal(5,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
    plot(time,zscore(squeeze(nanmean(CompSignal(6,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
%     plot(time,zscore(squeeze(nanmean(CompSignal(7,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
%     plot(time,zscore(squeeze(nanmean(CompSignal(8,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
%     plot(time,zscore(squeeze(nanmean(CompSignal(9,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
    
% line([0+6 0+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
% line([1+6 1+6], [-2 1.5], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
% line([3+6 3+6], [-2 1.5], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
% line([6+6 6+6], [-2 1.5], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
% line([8+6 8+6], [-2 1.5], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
% line([10+6 10+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
legend({'Cue'; 'Fix'; 'Stim'; 'Probe'; 'ITI'}, 'orientation', 'horizontal', 'location', 'NorthWest'); legend('boxoff')
title({'Temporal sequence of spatial networks';'(shifted by 6 s ~ canonical HRF)'})
xlabel('Time in seconds'); ylabel('Amplitude (z-scored)')
xlim([4, 25*.645]); ylim([-2 2])
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% pn.plotFolder = '/Users/kosciessa/Desktop/';
% figureName = 'TemporalSequence_colored';
% saveas(gca, [pn.plotFolder, figureName], 'epsc');
% saveas(gca, [pn.plotFolder, figureName], 'png');

%% align to block onset

CompSignal = [];
tmpTime = find(taskStructure.Regressors(:,1));
for indStim = 1:numel(tmpTime)
    CompSignal(1,indStim,:) = nanmean(ICcomps(T==1,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(2,indStim,:) = nanmean(ICcomps(T==5,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(3,indStim,:) = nanmean(ICcomps(T==4,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(4,indStim,:) = nanmean(ICcomps(T==3,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(5,indStim,:) = nanmean(ICcomps(T==16,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
end

time = [-3:30]*.645;
h = figure; hold on;
plot(time,zscore(squeeze(nanmean(CompSignal(1,:,:),2))), 'LineWidth', 2, 'Color', [.8 .8 .8])
plot(time,zscore(squeeze(nanmean(CompSignal(2,:,:),2))), 'LineWidth', 2, 'Color', [.7 .7 .7])
plot(time,zscore(squeeze(nanmean(CompSignal(3,:,:),2))), 'LineWidth', 2, 'Color', [.6 .6 .6])
plot(time,zscore(squeeze(nanmean(CompSignal(4,:,:),2))), 'LineWidth', 2, 'Color', [.5 .5 .5])
plot(time,zscore(squeeze(nanmean(CompSignal(5,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
line([0+6 0+6], [-2 2], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
line([4.5+6 4.5+6], [-2 2], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
legend({'1'; '5'; '4'; '3'; 'DMN'}, 'orientation', 'horizontal')
title('Temporal sequence of spatial networks')
xlabel('Time in s'); ylabel('Z-scored amplitude')
xlim([-3*.645, 30*.645]); ylim([-2.5 2.5])

%% retrieve all components and sort them by their peak

CompSignal = [];
tmpTime = find(taskStructure.Regressors(:,2));
for indStim = 1:numel(tmpTime)
    for indComp = 1:50
        CompSignal(indComp,indStim,:) = nanmean(ICcomps(T==indComp,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    end
end

curData = squeeze(nanmean(CompSignal,2));
[~, indSort] = max(curData,[],2);
[~, indSort] = sort(indSort, 'ascend');

figure; imagesc(time, [], curData(indSort,:))
hold on;
line([0+6 0+6], [0 100], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
line([1+6 1+6], [0 100], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
line([3+6 3+6], [0 100], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
line([6+6 6+6], [0 100], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
line([8+6 8+6], [0 100], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
line([10+6 10+6], [0 100], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
colormap('hot')

% without sorting

figure; imagesc(time, [], curData(:,:))
hold on;
line([0+6 0+6], [0 100], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
line([1+6 1+6], [0 100], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
line([3+6 3+6], [0 100], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
line([6+6 6+6], [0 100], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
line([8+6 8+6], [0 100], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
line([10+6 10+6], [0 100], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
colormap('hot')

figure; 
for indComp = 1:100
    cla; hold on;
    plot(time,curData(indComp,:)')
    line([0+6 0+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
    line([1+6 1+6], [-2 1.5], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
    line([3+6 3+6], [-2 1.5], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
    line([6+6 6+6], [-2 1.5], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
    line([8+6 8+6], [-2 1.5], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
    line([10+6 10+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
    title(['Component ', num2str(indComp)])
    pause(1);
end

%% TO DO: reverse alignment in FSL, use ANTs coregistration matrices


%% remove mean from raw time courses

ICcomps_demeaned = ICcomps_raw-repmat(mean(ICcomps_raw,2),1,1054);

%% for component 8 (thalamus) probe load effect

% get subjects and runs of the component
% segment according to load condition

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

for curIC = 1:50

    %curIC = 5;
    curComps = IdxComponent(find(T==curIC),:);
    curCompIdx = find(T==curIC);

    CompSignal = [];
    for indIC = 1:size(curComps,1)
        taskStructure = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/',IDs{curComps(indIC,1)},'_Run',num2str(curComps(indIC,2)),'_regressors.mat']);
        for indLoad = 1:4
            tmpTime = find(sum(taskStructure.Regressors(:,7:10),2)==indLoad);
            tmpSignal = [];
            for indStim = 1:numel(tmpTime)
                tmpSignal(indStim,:) = nanmedian(ICcomps_demeaned(curCompIdx(indIC),tmpTime(indStim)-4-3:tmpTime(indStim)-4+25),1);
            end
            CompSignal(1,indLoad,indIC,:) = nanmean(tmpSignal,1);
        end
    end

    % figure; imagesc(squeeze(CompSignal(1,1,:,:)))
    % figure; imagesc(squeeze(CompSignal(1,2,:,:)))
    % figure; imagesc(squeeze(CompSignal(1,3,:,:)))
    % figure; imagesc(squeeze(CompSignal(1,4,:,:)))

    time = [-3:25]*.645;
    h = figure; hold on;
        thresh = 5; xl = [0+6 1+6];
        p1 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.8 .8 .8]); p1.EdgeColor = 'none';p1.FaceAlpha = .3;
        thresh = 5; xl = [1+6 3+6];
        p2 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.7 .7 .7]); p2.EdgeColor = 'none';p2.FaceAlpha = .3;
        thresh = 5; xl = [3+6 6+6];
        p3 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.6 .6 .6]); p3.EdgeColor = 'none';p3.FaceAlpha = .3;
        thresh = 5; xl = [6+6 8+6];
        p4 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.5 .5 .5]); p4.EdgeColor = 'none';p4.FaceAlpha = .3;
        thresh = 5; xl = [8+6 10+6];
        p5 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[1 .3 .3]); p5.EdgeColor = 'none';p5.FaceAlpha = .3;
        cBrew = [.8 .8 .8; .7 .7 .7; .6 .6 .6; .5 .5 .5];
        % plot error bars
        for indCond = 1:4
            squeeze(nanmedian(CompSignal(1,indCond,:,:),3))
            condAvg = squeeze(nanmean(CompSignal(1,:,:,:),2));
            curData = squeeze(nanmean(CompSignal(1,indCond,:,:),2));
            curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
                dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                    'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
        end
%         plot(time,squeeze(nanmedian(CompSignal(1,1,:,:),3)), 'LineWidth', 2, 'Color', [.8 .8 .8])
%         plot(time,squeeze(nanmedian(CompSignal(1,2,:,:),3)), 'LineWidth', 2, 'Color', [.7 .7 .7])
%         plot(time,squeeze(nanmedian(CompSignal(1,3,:,:),3)), 'LineWidth', 2, 'Color', [.6 .6 .6])
%         plot(time,squeeze(nanmedian(CompSignal(1,4,:,:),3)), 'LineWidth', 2, 'Color', [.5 .5 .5])
    legend({'Cue'; 'Fix'; 'Stim'; 'Probe'; 'ITI'}, 'orientation', 'horizontal', 'location', 'SouthWest'); legend('boxoff')
    title({['Component ', num2str(curIC)]})
    xlabel('Time in seconds'); ylabel('Amplitude (demeaned)')
    xlim([4, 25*.645]); ylim([-5 5])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/A_goodComps/';
    figureName = ['D_LoadEffects_Comp', num2str(curIC)];
    saveas(h, [pn.plotFolder, figureName], 'png');
    close(h)
end


% time = [-3:20]*.645;
% h = figure; hold on;
%     plot(time,squeeze(nanmedian(CompSignal(1,1,:,:),3))-squeeze(nanmedian(nanmedian(CompSignal(1,:,:,:),3),2)), 'LineWidth', 2, 'Color', [.8 .8 .8])
%     plot(time,squeeze(nanmedian(CompSignal(1,2,:,:),3))-squeeze(nanmedian(nanmedian(CompSignal(1,:,:,:),3),2)), 'LineWidth', 2, 'Color', [.7 .7 .7])
%     plot(time,squeeze(nanmedian(CompSignal(1,3,:,:),3))-squeeze(nanmedian(nanmedian(CompSignal(1,:,:,:),3),2)), 'LineWidth', 2, 'Color', [.6 .6 .6])
%     plot(time,squeeze(nanmedian(CompSignal(1,4,:,:),3))-squeeze(nanmedian(nanmedian(CompSignal(1,:,:,:),3),2)), 'LineWidth', 2, 'Color', [.5 .5 .5])
% legend({'L1'; 'L2'; 'L3'; 'L4'}, 'orientation', 'horizontal', 'location', 'NorthWest'); legend('boxoff')
% title({'Temporal sequence of spatial networks';'(shifted by 6 s ~ canonical HRF)'})
% xlabel('Time in seconds'); ylabel('Amplitude (z-scored)')
% xlim([4, 20*.645]); %ylim([-2 2])
% set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% calculate SD of components

curIC = 12;
curComps = IdxComponent(find(T==curIC),:);
curCompIdx = find(T==curIC);

CompSignal = [];
for indIC = 1:size(curComps,1)
    taskStructure = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/',IDs{curComps(indIC,1)},'_Run',num2str(curComps(indIC,2)),'_regressors.mat']);
    for indLoad = 1:4
        tmpTime = find(sum(taskStructure.Regressors(:,7:10),2)==indLoad);
        tmpSignal = [];
        for indStim = 1:numel(tmpTime)
            tmpSignal(indStim,:) = nanmean(ICcomps_demeaned(curCompIdx(indIC),tmpTime(indStim)-4+10:tmpTime(indStim)-4+20),1);
        end
        %CompSignal(1,indLoad,indIC,:) = nanmean(nanstd(tmpSignal,[],2),1);
        CompSignal(1,indLoad,indIC,:) = nanmean(nanmean(tmpSignal,2),1);
    end
end

figure; bar(squeeze(nanmedian(CompSignal,3)))


[h, p] = ttest(squeeze(CompSignal(1,1,:)), squeeze(CompSignal(1,4,:)))
[h, p] = ttest(squeeze(CompSignal(1,1,:)), squeeze(CompSignal(1,2,:)))
[h, p] = ttest(squeeze(CompSignal(1,1,:)), squeeze(CompSignal(1,3,:)))
[h, p] = ttest(squeeze(CompSignal(1,2,:)), squeeze(CompSignal(1,3,:)))
[h, p] = ttest(squeeze(CompSignal(1,3,:)), squeeze(CompSignal(1,4,:)))

%% calculate the mean difference map between C2 and C8

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));
% load nifty with spatial components
C2 = load_untouch_nii(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/A_goodComps/Cluster2.nii']);
C8 = load_untouch_nii(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/A_goodComps/Cluster8.nii']);
% save as nifty
tempNii = C2;
%tempNii.img = (nanmean(C8.img,4)-nanmean(C2.img,4));
% spatial z-score
% tempNii.img = (nanmean(reshape(zscore(reshape(C8.img,74*74*40,[]),[],1),74,74,40,[]),4)-...
%     nanmean(reshape(zscore(reshape(C2.img,74*74*40,[]),[],1),74,74,40,[]),4));
tempNii.img = (reshape(zscore(reshape(nanmean(C8.img,4),74*74*40,[]),[],1),74,74,40,[])-...
    reshape(zscore(reshape(nanmean(C2.img,4),74*74*40,[]),[],1),74,74,40,[]));
tempNii.hdr.dime.dim(5) = size(tempNii.img,4);
save_untouch_nii(tempNii,['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/A_goodComps/', 'C8_2_Diff.nii'])


%% get individual stimulus-related FPN activations by subject

N_all_unique = unique(IdxComponent(ismember(T, [8,12,14,34]),1:2), 'rows');

curComps = find(ismember(T, [3,6,8,10,12,14,16,19,30,34]));
%curComps = find(ismember(T, [8,12,14,34]));
%curComps = find(ismember(T, [34]));
curComps = find(ismember(T, [8]));
%curComps = find(ismember(T, [1,2])); % Stim Onset
%curComps = find(ismember(T, [13,15,28])); % DMN

StimFPNbySub = [];
for indID = 1:size(IDs)
    curTrls = find(IdxComponent(curComps,1) == indID);
    if isempty(curTrls)
        StimFPNbySub(indID,:,:) = NaN(4,29);
    else
        curIdx = curComps(curTrls);
        curICs = IdxComponent(curIdx,:);
        tmpSignalByIC = NaN(4,size(curICs,1),29);
        for indIC = 1:size(curICs,1)
            taskStructure = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/',IDs{curICs(indIC,1)},'_Run',num2str(curICs(indIC,2)),'_regressors.mat']);
            for indLoad = 1:4
                tmpTime = find(sum(taskStructure.Regressors(:,7:10),2)==indLoad);
                tmpSignal = [];
                for indStim = 1:numel(tmpTime)
                    tmpSignal(indStim,:) = ICcomps_demeaned(curIdx(indIC),tmpTime(indStim)-4-3:tmpTime(indStim)-4+25);
                end
                tmpSignalByIC(indLoad,indIC,:) = nanmean(tmpSignal,1);
            end
        end
        StimFPNbySub(indID,:,:) = squeeze(nanmean(tmpSignalByIC,2));
    end
end

figure; imagesc(squeeze(nanmean(StimFPNbySub,2)))

time = [-3:25]*.645;
    h = figure; hold on;
        thresh = 5; xl = [0+6 1+6];
        p1 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.8 .8 .8]); p1.EdgeColor = 'none';p1.FaceAlpha = .3;
        thresh = 5; xl = [1+6 3+6];
        p2 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.7 .7 .7]); p2.EdgeColor = 'none';p2.FaceAlpha = .3;
        thresh = 5; xl = [3+6 6+6];
        p3 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.6 .6 .6]); p3.EdgeColor = 'none';p3.FaceAlpha = .3;
        thresh = 5; xl = [6+6 8+6];
        p4 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.5 .5 .5]); p4.EdgeColor = 'none';p4.FaceAlpha = .3;
        thresh = 5; xl = [8+6 10+6];
        p5 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[1 .3 .3]); p5.EdgeColor = 'none';p5.FaceAlpha = .3;
        cBrew = [.8 .8 .8; .7 .7 .7; .6 .6 .6; .5 .5 .5];
        % plot error bars
        for indCond = 1:4
            condAvg = squeeze(nanmean(StimFPNbySub(:,:,:),2));
            curData = squeeze(nanmean(StimFPNbySub(:,indCond,:),2));
            curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
            standError = nanstd(curData,1)./sqrt(size(curData,1));
                dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                    'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
        end
    legend({'Cue'; 'Fix'; 'Stim'; 'Probe'; 'ITI'}, 'orientation', 'horizontal', 'location', 'SouthWest'); legend('boxoff')
    title({['Trajectory across components']})
    xlabel('Time in seconds'); ylabel('Amplitude (demeaned)')
    xlim([4, 25*.645]); ylim([-2.5 2])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


figure; hold on;
plot(squeeze(nanmedian(nanmean(StimFPNbySub(1:47,1,:),2),1)), 'Color', [1 .3 .3], 'LineWidth', 2)
plot(squeeze(nanmedian(nanmean(StimFPNbySub(1:47,2,:),2),1)), 'Color', [1 .5 .5], 'LineWidth', 2)
plot(squeeze(nanmedian(nanmean(StimFPNbySub(1:47,3,:),2),1)), 'Color', [1 .7 .7], 'LineWidth', 2)
plot(squeeze(nanmedian(nanmean(StimFPNbySub(1:47,4,:),2),1)), 'Color', [1 .9 .9], 'LineWidth', 2)
legend({'L1'; 'L2'; 'L3'; 'L4'})

% plot(squeeze(nanmedian(nanmean(StimFPNbySub(48:end,1,:),2),1)),'--', 'Color', [1 .3 .3], 'LineWidth', 2)
% plot(squeeze(nanmedian(nanmean(StimFPNbySub(48:end,2,:),2),1)),'--', 'Color', [1 .5 .5], 'LineWidth', 2)
% plot(squeeze(nanmedian(nanmean(StimFPNbySub(48:end,3,:),2),1)),'--', 'Color', [1 .7 .7], 'LineWidth', 2)
% plot(squeeze(nanmedian(nanmean(StimFPNbySub(48:end,4,:),2),1)),'--', 'Color', [1 .9 .9], 'LineWidth', 2)
% legend({'L1'; 'L2'; 'L3'; 'L4'})

[~, p] = ttest(nanmedian(nanmean(StimFPNbySub(:,1,19:21),2),3), nanmedian(nanmean(StimFPNbySub(:,2,19:21),2),3))
[~, p] = ttest(nanmedian(nanmean(StimFPNbySub(:,2,19:21),2),3), nanmedian(nanmean(StimFPNbySub(:,3,19:21),2),3))
[~, p] = ttest(nanmedian(nanmean(StimFPNbySub(:,2,19:21),2),3), nanmedian(nanmean(StimFPNbySub(:,4,19:21),2),3))
[~, p] = ttest(nanmedian(nanmean(StimFPNbySub(:,3,19:21),2),3), nanmedian(nanmean(StimFPNbySub(:,4,19:21),2),3))


[~, p] = ttest(nanstd(nanmean(StimFPNbySub(:,1,10:end),2),[],3), nanstd(nanmean(StimFPNbySub(:,2,10:end),2),[],3))
[~, p] = ttest(nanstd(nanmean(StimFPNbySub(:,2,10:end),2),[],3), nanstd(nanmean(StimFPNbySub(:,3,10:end),2),[],3))
[~, p] = ttest(nanstd(nanmean(StimFPNbySub(:,2,10:end),2),[],3), nanstd(nanmean(StimFPNbySub(:,4,10:end),2),[],3))
[~, p] = ttest(nanstd(nanmean(StimFPNbySub(:,3,10:end),2),[],3), nanstd(nanmean(StimFPNbySub(:,4,10:end),2),[],3))

figure; bar([nanmean(nanstd(nanmean(StimFPNbySub(:,1,10:end),2),[],3));...
    nanmean(nanstd(nanmean(StimFPNbySub(:,2,10:end),2),[],3));...
    nanmean(nanstd(nanmean(StimFPNbySub(:,3,10:end),2),[],3));...
    nanmean(nanstd(nanmean(StimFPNbySub(:,4,10:end),2),[],3))])

figure; imagesc(time,[],squeeze(nanmean(StimFPNbySub(:,2:4,:),2))-squeeze(nanmean(StimFPNbySub(:,1,:),2)))
figure; plot(time, squeeze(nanmean(squeeze(nanmean(StimFPNbySub(:,2:4,:),2))-squeeze(nanmean(StimFPNbySub(:,1,:),2)),1)))

% attempt to correlate with attentional modulation factor

AF = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat');

idx_AF = ismember(AF.EEGAttentionFactor.IDs,IDs);
idx_IDs = ismember(IDs,AF.EEGAttentionFactor.IDs);

figure
%tmp_data = squeeze(nanmean(StimFPNbySub(:,4,:),2))-squeeze(nanmean(StimFPNbySub(:,1,:),2));
tmp_data = squeeze(nanmedian(nanmedian(StimFPNbySub(:,1:4,20:24),3),2));
%tmp_data = nanmedian(tmp_data(:,20:24),2);
scatter(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF),tmp_data(idx_IDs), 'filled'); lsline();
[r, p] = corrcoef(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF),tmp_data(idx_IDs), 'rows', 'complete')
ylabel('Median stimulus activation FPN'); xlabel('EEG Attentional modulation factor')
title({'Across all subjects, EEG modulation correalates with FPN stim activation'; ['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]})


AF.EEGAttentionFactor.PCAalphaGamma(37) = NaN;

figure
% tmp_data = squeeze(nanmean(StimFPNbySub(:,2:4,:),2))-squeeze(nanmean(StimFPNbySub(:,1,:),2));
% %tmp_data = squeeze(nanmedian(StimFPNbySub(:,1:4,:),2));
% tmp_data = nanstd(tmp_data(:,10:end),[],2);
tmp_data = squeeze(nanmean(nanstd(StimFPNbySub(:,2:4,10:end),[],3),2))-squeeze(nanmean(nanstd(StimFPNbySub(:,1,10:end),[],3),2));
%tmp_data = squeeze(nanmean(nanstd(StimFPNbySub(:,1:4,10:end),[],3),2));
scatter(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF),tmp_data(idx_IDs), 'filled'); lsline();
[r, p] = corrcoef(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF),tmp_data(idx_IDs), 'rows', 'complete')
ylabel('FPN activation SD 234-1'); xlabel('EEG Attentional modulation factor')
title({'Across all subjects, EEG modulation correlates with FPN SD'; ['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]})


% plot separately for OA + YA

sharedIDs = intersect(AF.EEGAttentionFactor.IDs,IDs);

idx_AF_YA = ismember(AF.EEGAttentionFactor.IDs,sharedIDs(1:42));
idx_IDs_YA = ismember(IDs,sharedIDs(1:42));
idx_AF_OA = ismember(AF.EEGAttentionFactor.IDs,sharedIDs(43:end));
idx_IDs_OA = ismember(IDs,sharedIDs(43:end));

figure; hold on;
%tmp_data = (squeeze(nanmean(StimFPNbySub(:,2:4,:),2))-squeeze(nanmean(StimFPNbySub(:,1,:),2)));
tmp_data = squeeze(nanmedian(StimFPNbySub(:,1:4,:),2));
tmp_data = nanmean(tmp_data(:,20:24),2);
scatter(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_YA),tmp_data(idx_IDs_YA), 'filled'); lsline();
[~, p] = corrcoef(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_YA),tmp_data(idx_IDs_YA), 'rows', 'complete')

scatter(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_OA),tmp_data(idx_IDs_OA), 'filled'); lsline();
[~, p] = corrcoef(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_OA),tmp_data(idx_IDs_OA), 'rows', 'complete')

% plot separately for OA + YA (STD)

sharedIDs = intersect(AF.EEGAttentionFactor.IDs,IDs);

idx_AF_YA = ismember(AF.EEGAttentionFactor.IDs,sharedIDs(1:42));
idx_IDs_YA = ismember(IDs,sharedIDs(1:42));
idx_AF_OA = ismember(AF.EEGAttentionFactor.IDs,sharedIDs(43:end));
idx_IDs_OA = ismember(IDs,sharedIDs(43:end));

figure; hold on;
tmp_data = (squeeze(nanmean(StimFPNbySub(:,2:4,:),2))-squeeze(nanmean(StimFPNbySub(:,1,:),2)));
%tmp_data = squeeze(nanmedian(StimFPNbySub(:,1:4,:),2));
tmp_data = nanstd(tmp_data(:,10:end),[],2);
scatter(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_YA),tmp_data(idx_IDs_YA), 'filled'); lsline();
[~, p] = corrcoef(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_YA),tmp_data(idx_IDs_YA), 'rows', 'complete')

scatter(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_OA),tmp_data(idx_IDs_OA), 'filled'); lsline();
[~, p] = corrcoef(AF.EEGAttentionFactor.PCAalphaGamma(idx_AF_OA),tmp_data(idx_IDs_OA), 'rows', 'complete')

