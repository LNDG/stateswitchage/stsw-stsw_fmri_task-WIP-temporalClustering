% try to cluster ICA components across subjects to extract task structure components

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

IdxComponent = []; ICcomps = [];
for indID = 1:numel(IDs)
    disp(['Processing ID ', IDs{indID}]);
    for indRun = 1:4
        for i = 1:30
            try
               t=load([pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/',IDs{indID},'/preproc2/run-',num2str(indRun),'/FEAT.feat/filtered_func_data.ica/report/t' num2str(i) '.txt']);
               if numel(t) == 1054
                   ICcomps = cat(1,ICcomps,t');
                   IdxComponent = cat(1,IdxComponent, [indID,indRun,i]);
               else
                   disp('Error: size does not match');
               end
            catch
                disp('Error: an error has occured');
            end
        end
    end
end

%% calculate temporal correlation

corrMat = corrcoef(ICcomps');
Z = linkage(corrMat,'complete','correlation');

Z2 = linkage(ICcomps,'complete','correlation');

%% save info

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/A_clusterData.mat', 'Z2', 'Z', 'ICcomps', 'IdxComponent')

numComps = 100;
T = cluster(Z2,'maxclust',numComps);
%[~, sortVal] = sort(T, 'ascend');
%figure; imagesc(corrMat(sortVal,sortVal),[-.3 .3])
% figure; 
% for indComp = 1:numComps
% 	subplot(numComps,1,indComp); plot(nanmean(ICcomps(T==indComp,:)));
% end

% sort T by correlation magnitude within cluster
for indCluster = 1:numComps
    CorrVal(indCluster) = nanmean(nanmean(corrMat(T == indCluster,T == indCluster)));
end

[sortV, sortI] = sort(CorrVal,'descend');
T2 = T;
for indCluster = 1:numComps
   T(T2==sortI(indCluster)) = indCluster;
end

[~, sortVal] = sort(T, 'ascend');
figure; imagesc(corrMat(sortVal,sortVal),[-.3 .3])

figure;
for indComp = 1:50
    imagesc(ICcomps(T==indComp,:),[-1 1])
    pause(1)
end
imagesc(ICcomps(T==8,:),[-1 1])
imagesc(ICcomps(T==24,:),[-1 1])
imagesc(ICcomps(T==10,:),[-1 1])

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(500,'RdBu');
% cBrew = flipud(cBrew);

h = figure('units','normalized','position',[.1 .1 .7 .8]);
for indComp = 1:16
    subplot(4,4,indComp)
    imagesc(ICcomps(T==indComp,:),[-1 1])
    ylabel('IC count'); xlabel('Time (vol.)')
    title(['Cluster ', num2str(indComp)])
end
suptitle('Temporal structure in spatial ICs')
colormap(parula)
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/J_temporalClustering/C_figures/';
figureName = 'TemporalICs';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


figure; hold on;
plot(nanmean(ICcomps(T==1,:)))
plot(nanmean(ICcomps(T==10,:)))

h = figure; hold on;
plot(nanmean(ICcomps(T==1,:)), 'LineWidth', 2, 'Color', [.8 .8 .8])
plot(nanmean(ICcomps(T==5,:)), 'LineWidth', 2, 'Color', [.7 .7 .7])
plot(nanmean(ICcomps(T==4,:)), 'LineWidth', 2, 'Color', [.6 .6 .6])
plot(nanmean(ICcomps(T==3,:)), 'LineWidth', 2, 'Color', [.5 .5 .5])
plot(nanmean(ICcomps(T==16,:)), 'LineWidth', 2, 'Color', [1 .3 .3])
legend({'1'; '5'; '4'; '3'; 'DMN'})
title('Temporal sequence of spatial networks')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Users/kosciessa/Desktop/';
figureName = 'TemporalSequence';
saveas(gca, [pn.plotFolder, figureName], 'epsc');
saveas(gca, [pn.plotFolder, figureName], 'png');


figure; imagesc(Z)
figure;dendrogram(Z)
T1 = clusterdata(corrMat, 5);

%% check for unique properties of state 4

curComps = find(T==4);
curComp = IdxComponent(curComps,:,:);

figure; histogram(curComp(:,2))

%% extract topographies of different clusters for pre-labeling

%IdxComponent(curComps(1:10),:,:)

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));

for indCluster = 1:50
    curComps = find(T==indCluster);
    ClusterTopo = [];
    for indImage = 1:numel(curComps)
        disp(['Processing image ', num2str(indImage), ' of ', num2str(numel(curComps))]);
        curComp = IdxComponent(curComps(indImage),:,:);
        % load nifty with spatial components
        dataFile = [pn.root, 'preproc/B_data/D_preproc/',IDs{curComp(1,1)},'/preproc2/run-',num2str(curComp(1,2)),'/FEAT.feat/filtered_func_data.ica/melodic_IC.nii.gz'];
        try
            tempNii = load_untouch_nii([dataFile]);
            % encode in matrix
            ClusterTopo(:,:,:,indImage) = squeeze(tempNii.img(:,:,:,curComp(1,3)));
        catch
            disp('Error')
        end
    end
    % save as nifty
    tempNii.img = ClusterTopo;
    tempNii.hdr.dime.dim(5) = size(ClusterTopo,4);
    save_untouch_nii(tempNii,['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/', 'Cluster',num2str(indCluster),'.nii'])
end

%% create mean maps for each component

for indCluster = 1:50
    tempNii = load_untouch_nii(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/', 'Cluster',num2str(indCluster),'.nii']);
    tempNii.img = nanmean(tempNii.img,4);
    tempNii.hdr.dime.dim(5) = 1;
    save_untouch_nii(tempNii,['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/', 'Cluster',num2str(indCluster),'_mean.nii']);
end

%% check how many components in the relevant categories were labelled as noise

goodComps = [1,3,4,5,16];
goodComps = [1];
curComps = find(ismember(T,goodComps));
curComp = IdxComponent(curComps,:,:);
        
pn.root = '/Volumes/LNDG/Projects/StateSwitch/';

ErrorMatrix = []; CorrectMatrix = [];
for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}]);
    for indRun = 1:4
        HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/',IDs{indID},'/preproc2/run-',num2str(indRun),'/FEAT.feat/hand_labels_noise.txt'];
        [~,IC_noise] = system(['tail -n -1 ' HLN]);
        IC_noise = str2num(IC_noise);
        IC_good = setdiff(1:30,IC_noise);
        % check how many of the 'good components' were labeled as noise
        tmpICs = find(curComp(:,1) == indID & curComp(:,2) == indRun);
        tmpICs = curComp(tmpICs,3);
        tmpError = ismember(tmpICs, IC_noise);
        tmpCorrect = ismember(tmpICs, IC_good);
        tmpDetected = tmpICs(tmpError);
        tmpDetectedC = tmpICs(tmpCorrect);
        if ~isempty(tmpDetected)
            for indDet = 1:numel(tmpDetected)
                ErrorMatrix = cat(1,ErrorMatrix, [indID, indRun, tmpDetected(indDet)]);
            end
        end; clear tmpDetected;
        if ~isempty(tmpDetectedC)
            for indDet = 1:numel(tmpDetectedC)
                CorrectMatrix = cat(1,CorrectMatrix, [indID, indRun, tmpDetectedC(indDet)]);
            end
        end; clear tmpDetectedC;
    end % run
end % id

size(ErrorMatrix)
size(CorrectMatrix)

% every single image of those labeled as artifact also looks like an artifact!

%% create spatial maps of components labeled as noise

for indGoodComp = 1:50
    curComps = find(ismember(T,indGoodComp));
    curComp = IdxComponent(curComps,:,:);

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/';

    ErrorMatrix = []; CorrectMatrix = [];
    for indID = 1:numel(IDs)
        disp(['Processing subject ', IDs{indID}]);
        for indRun = 1:4
            HLN=[pn.root, 'dynamic/data/mri/task/preproc/B_data/D_preproc/',IDs{indID},'/preproc2/run-',num2str(indRun),'/FEAT.feat/hand_labels_noise.txt'];
            [~,IC_noise] = system(['tail -n -1 ' HLN]);
            IC_noise = str2num(IC_noise);
            IC_good = setdiff(1:30,IC_noise);
            % check how many of the 'good components' were labeled as noise
            tmpICs = find(curComp(:,1) == indID & curComp(:,2) == indRun);
            tmpICs = curComp(tmpICs,3);
            tmpError = ismember(tmpICs, IC_noise);
            tmpCorrect = ismember(tmpICs, IC_good);
            tmpDetected = tmpICs(tmpError);
            tmpDetectedC = tmpICs(tmpCorrect);
            if ~isempty(tmpDetected)
                for indDet = 1:numel(tmpDetected)
                    ErrorMatrix = cat(1,ErrorMatrix, [indID, indRun, tmpDetected(indDet)]);
                end
            end; clear tmpDetected;
            if ~isempty(tmpDetectedC)
                for indDet = 1:numel(tmpDetectedC)
                    CorrectMatrix = cat(1,CorrectMatrix, [indID, indRun, tmpDetectedC(indDet)]);
                end
            end; clear tmpDetectedC;
        end % run
    end % id
    
    %% grab relevant volumes and create topography
    % load nifty with spatial components
    ClusterTopo = [];
    for indImage = 1:size(ErrorMatrix,1)
        curData = ErrorMatrix(indImage,:);
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
        dataFile = [pn.root, 'preproc/B_data/D_preproc/',IDs{curData(1)},'/preproc2/run-',num2str(curData(2)),'/FEAT.feat/filtered_func_data.ica/melodic_IC.nii.gz'];
        try
            tempNii = load_untouch_nii([dataFile]);
            % encode in matrix
            ClusterTopo(:,:,:,indImage) = squeeze(tempNii.img(:,:,:,curData(1,3)));
        catch
            disp('Error')
        end
    end
    % save as nifty
    tempNii.img = ClusterTopo;
    tempNii.hdr.dime.dim(5) = size(ClusterTopo,4);
    save_untouch_nii(tempNii,['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/G_temporalClustering/', 'Cluster',num2str(indGoodComp),'_noise.nii'])

end

%% indicate task onsets, cut into trials

taskStructure = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/1117_Run1_regressors.mat');
taskStructure.Regressors
taskStructure.RegressorInfo

h = figure; hold on;
plot(nanmean(ICcomps(T==1,:)), 'LineWidth', 2, 'Color', [.8 .8 .8])
plot(nanmean(ICcomps(T==5,:)), 'LineWidth', 2, 'Color', [.7 .7 .7])
plot(nanmean(ICcomps(T==4,:)), 'LineWidth', 2, 'Color', [.6 .6 .6])
plot(nanmean(ICcomps(T==3,:)), 'LineWidth', 2, 'Color', [.5 .5 .5])
plot(nanmean(ICcomps(T==16,:)), 'LineWidth', 2, 'Color', [1 .3 .3])
plot(taskStructure.Regressors(:,1))
plot(taskStructure.Regressors(:,2))
legend({'1'; '5'; '4'; '3'; 'DMN'})
title('Temporal sequence of spatial networks')

CompSignal = [];
tmpTime = find(taskStructure.Regressors(:,2));
for indStim = 1:numel(tmpTime)
    CompSignal(1,indStim,:) = nanmean(ICcomps(T==1,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(2,indStim,:) = nanmean(ICcomps(T==5,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(3,indStim,:) = nanmean(ICcomps(T==4,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(4,indStim,:) = nanmean(ICcomps(T==3,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    CompSignal(5,indStim,:) = nanmean(ICcomps(T==16,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
end

time = [-3:25]*.645;
h = figure; hold on;
plot(time,zscore(squeeze(nanmean(CompSignal(1,:,:),2))), 'LineWidth', 2, 'Color', [.8 .8 .8])
plot(time,zscore(squeeze(nanmean(CompSignal(2,:,:),2))), 'LineWidth', 2, 'Color', [.7 .7 .7])
plot(time,zscore(squeeze(nanmean(CompSignal(3,:,:),2))), 'LineWidth', 2, 'Color', [.6 .6 .6])
plot(time,zscore(squeeze(nanmean(CompSignal(4,:,:),2))), 'LineWidth', 2, 'Color', [.5 .5 .5])
plot(time,zscore(squeeze(nanmean(CompSignal(5,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
line([0 0], [-2 2], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
line([1 1], [-2 2], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
line([3 3], [-2 2], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
line([6 6], [-2 2], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
line([8 8], [-2 2], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
line([10 10], [-2 2], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
legend({'1'; '5'; '4'; '3'; 'DMN'}, 'orientation', 'horizontal')
title('Temporal sequence of spatial networks')
xlabel('Time in s'); ylabel('Z-scored amplitude')
xlim([-3*.645, 25*.645])

time = [-3:25]*.645;
h = figure; hold on;
    thresh = 2; xl = [0+6 1+6];
    p1 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.8 .8 .8]); p1.EdgeColor = 'none';p1.FaceAlpha = .3;
    thresh = 2; xl = [1+6 3+6];
    p2 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.7 .7 .7]); p2.EdgeColor = 'none';p2.FaceAlpha = .3;
    thresh = 2; xl = [3+6 6+6];
    p3 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.6 .6 .6]); p3.EdgeColor = 'none';p3.FaceAlpha = .3;
    thresh = 2; xl = [6+6 8+6];
    p4 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.5 .5 .5]); p4.EdgeColor = 'none';p4.FaceAlpha = .3;
    thresh = 2; xl = [8+6 10+6];
    p5 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[1 .3 .3]); p5.EdgeColor = 'none';p5.FaceAlpha = .3;
    plot(time,zscore(squeeze(nanmean(CompSignal(1,:,:),2))), 'LineWidth', 2, 'Color', [.8 .8 .8])
    plot(time,zscore(squeeze(nanmean(CompSignal(2,:,:),2))), 'LineWidth', 2, 'Color', [.7 .7 .7])
    plot(time,zscore(squeeze(nanmean(CompSignal(3,:,:),2))), 'LineWidth', 2, 'Color', [.6 .6 .6])
    plot(time,zscore(squeeze(nanmean(CompSignal(4,:,:),2))), 'LineWidth', 2, 'Color', [.5 .5 .5])
    plot(time,zscore(squeeze(nanmean(CompSignal(5,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
% line([0+6 0+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
% line([1+6 1+6], [-2 1.5], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
% line([3+6 3+6], [-2 1.5], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
% line([6+6 6+6], [-2 1.5], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
% line([8+6 8+6], [-2 1.5], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
% line([10+6 10+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
legend({'Cue'; 'Fix'; 'Stim'; 'Probe'; 'ITI'}, 'orientation', 'horizontal', 'location', 'NorthWest'); legend('boxoff')
title({'Temporal sequence of spatial networks';'(shifted by 6 s ~ canonical HRF)'})
xlabel('Time in seconds'); ylabel('Amplitude (z-scored)')
xlim([4, 25*.645]); ylim([-2 2])
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Users/kosciessa/Desktop/';
figureName = 'TemporalSequence_colored';
saveas(gca, [pn.plotFolder, figureName], 'epsc');
saveas(gca, [pn.plotFolder, figureName], 'png');

%% align to block onset

CompSignal = [];
tmpTime = find(taskStructure.Regressors(:,1));
for indStim = 1:numel(tmpTime)
    CompSignal(1,indStim,:) = nanmean(ICcomps(T==1,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(2,indStim,:) = nanmean(ICcomps(T==5,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(3,indStim,:) = nanmean(ICcomps(T==4,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(4,indStim,:) = nanmean(ICcomps(T==3,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
    CompSignal(5,indStim,:) = nanmean(ICcomps(T==16,tmpTime(indStim)-3:tmpTime(indStim)+30),1);
end

time = [-3:30]*.645;
h = figure; hold on;
plot(time,zscore(squeeze(nanmean(CompSignal(1,:,:),2))), 'LineWidth', 2, 'Color', [.8 .8 .8])
plot(time,zscore(squeeze(nanmean(CompSignal(2,:,:),2))), 'LineWidth', 2, 'Color', [.7 .7 .7])
plot(time,zscore(squeeze(nanmean(CompSignal(3,:,:),2))), 'LineWidth', 2, 'Color', [.6 .6 .6])
plot(time,zscore(squeeze(nanmean(CompSignal(4,:,:),2))), 'LineWidth', 2, 'Color', [.5 .5 .5])
plot(time,zscore(squeeze(nanmean(CompSignal(5,:,:),2))), 'LineWidth', 2, 'Color', [1 .3 .3])
line([0+6 0+6], [-2 2], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
line([4.5+6 4.5+6], [-2 2], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
legend({'1'; '5'; '4'; '3'; 'DMN'}, 'orientation', 'horizontal')
title('Temporal sequence of spatial networks')
xlabel('Time in s'); ylabel('Z-scored amplitude')
xlim([-3*.645, 30*.645]); ylim([-2.5 2.5])

%% retrieve all components and sort them by their peak

CompSignal = [];
tmpTime = find(taskStructure.Regressors(:,2));
for indStim = 1:numel(tmpTime)
    for indComp = 1:100
        CompSignal(indComp,indStim,:) = nanmean(ICcomps(T==indComp,tmpTime(indStim)-3:tmpTime(indStim)+25),1);
    end
end

curData = squeeze(nanmean(CompSignal,2));
[~, indSort] = max(curData,[],2);
[~, indSort] = sort(indSort, 'ascend');

figure; imagesc(time, [], curData(indSort,:))
hold on;
line([0+6 0+6], [0 100], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
line([1+6 1+6], [0 100], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
line([3+6 3+6], [0 100], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
line([6+6 6+6], [0 100], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
line([8+6 8+6], [0 100], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
line([10+6 10+6], [0 100], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')

figure; 
for indComp = 1:100
    cla; hold on;
    plot(time,curData(indComp,:)')
    line([0+6 0+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
    line([1+6 1+6], [-2 1.5], 'Color', [.7 .7 .7], 'LineWidth', 2, 'LineStyle', '--')
    line([3+6 3+6], [-2 1.5], 'Color', [.6 .6 .6], 'LineWidth', 2, 'LineStyle', '--')
    line([6+6 6+6], [-2 1.5], 'Color', [.5 .5 .5], 'LineWidth', 2, 'LineStyle', '--')
    line([8+6 8+6], [-2 1.5], 'Color', [1 .3 .3], 'LineWidth', 2, 'LineStyle', '--')
    line([10+6 10+6], [-2 1.5], 'Color', [.8 .8 .8], 'LineWidth', 2, 'LineStyle', '--')
    title(['Component ', num2str(indComp)])
    pause(1);
end

%% TO DO: reverse alignment in FSL, use ANTs coregistration matrices